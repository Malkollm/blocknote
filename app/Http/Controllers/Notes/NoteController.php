<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller;
use App\NoteModel;


class NoteController extends Controller
{
    public function allNotes(){
        return response()->json(NoteModel::get(), 200);
    }

    public function noteByID($id){
        return response()->json(NoteModel::find($id), 200);
    }
}
